FROM golang:alpine as builder

WORKDIR /root/go/src/gitlab.com/file_transfer/worker
ENV GOPATH=/root/go
COPY ./ .
RUN apk add --update git  \
    && go get -v -t ./... \
    && go build -o /root/upload-worker -v main.go

FROM alpine
WORKDIR /app
COPY --from=builder /root/upload-worker /app/
RUN apk add --update ca-certificates \
    && adduser -u 1000 worker -D
ENTRYPOINT ./upload-worker

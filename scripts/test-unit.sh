#!/bin/bash
mkdir -p .tmp/upload
rm -rf .tmp/upload/* 

echo "This is my test file" > .tmp/upload/error
echo "This is my test file" > .tmp/upload/unit.txt

go test -v -coverprofile .tmp/cp.out -timeout 30s ./worker/...
package worker

import (
	"fmt"

	"github.com/caarlos0/env"
)

type WorkerOpts struct {
	MaxUploads uint16 `env:"MAX_UPLOADS" envDefault:"4"`
	BasePath   string `env:"UPLOAD_DIR" envDefault:"/tmp"`
	LogFile    string `env:"LOG_FILE" envDefault:"/tmp/worker.log"`
	LogLevel   string `env:"LOG_LEVEL" envDefault:"info"`
	Bucket     string `env:"UPLOAD_BUCKET"`
	MsgConfig  *MsgConfig
}

// Config for message bus connection
type MsgConfig struct {
	TLS  bool   `env:"MSG_TLS"`
	User string `env:"MSG_USER"`
	Pass string `env:"MSG_PASS"`
	Host string `env:"MSG_HOST" envDefault:"localhost"`
	Port int    `env:"MSG_PORT" envDefault:"4222"`
}

func GetOpts() *WorkerOpts {
	msgC := &MsgConfig{}
	err := env.Parse(msgC)
	if err != nil {
		// This should never happen, hence panic
		panic(fmt.Sprintf("Unable to parse message bus config: %v", err))
	}
	opts := &WorkerOpts{
		MsgConfig: msgC,
	}
	err = env.Parse(opts)
	if err != nil {
		panic(fmt.Sprintf("Unable to parse main config: %v", err))
	}
	return opts
}

package worker

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/rs/zerolog"
)

var hostname, _ = os.Hostname()

type Worker struct {
	Config    *WorkerOpts
	Logger    *zerolog.Logger
	logCloser io.Closer
	mb        messageBusI
	uploader  *uploader
	uploads   uint16
	upQueue   *uploadQueue
	mu        sync.Mutex
}

// Create a new worker instance
func New() (*Worker, error) {
	w := Worker{}
	w.Config = GetOpts()

	l := wLogger{
		LogFile:  w.Config.LogFile,
		LogLevel: w.Config.LogLevel,
	}
	logger, closer, err := l.startLogger()
	if err != nil {
		return nil, err
	}
	w.Logger, w.logCloser = logger, closer
	w.uploader = newUploader(w.Config.Bucket)
	return &w, nil
}

// Connect to message bus and start responding to file uploads
func (w *Worker) Start() error {
	w.Logger.Info().Msg("Starting worker")
	if w.mb == nil {
		w.mb = &messageBus{
			config:       w.Config.MsgConfig,
			msgConsumer:  w,
			statChanSize: int(w.Config.MaxUploads) * 4,
		}
	}
	w.Logger.Debug().Msg("Connecting to message bus")
	err := w.mb.Connect()
	if err != nil {
		return fmt.Errorf(eMap["msgBusConnect"], err)
	}

	w.Logger.Debug().Msg("Subscribing to worker queue")
	err = w.mb.Subscribe()
	if err != nil {
		return fmt.Errorf(eMap["msgBusSubcribe"], err)
	}
	w.upQueue = &uploadQueue{}
	go w.pollUploadQueue()
	w.Logger.Info().Msg("Worker started")
	return nil
}

func (w *Worker) Stop() {
	w.Logger.Info().Msg("Stoping worker")
	if w.mb != nil {
		w.Logger.Debug().Msg("Closing connection to message bus")
		w.mb.Close()
	}
	if w.logCloser != nil {
		w.logCloser.Close()
	}
}

// Callback function that will execute with every message from dispatcher
func (w *Worker) subCB(msg *DispatchMsg, err error) {
	if err != nil {
		w.Logger.Error().Err(err).Msg(eMap["msgDisp"])
		return
	}
	w.Logger.Debug().Msgf(
		"Received message from dispatcher, action: '%s', file: '%s'",
		msg.Action, msg.File)

	// Action validation already happens in QueueSubscribe CB
	switch msg.Action {
	case "fileUpload":
		w.upQueue.push(msg)
	}
}

func (w *Worker) changeUploadCount(ch int) {
	w.mu.Lock()
	defer w.mu.Unlock()
	switch ch {
	case 1:
		w.uploads++
	case -1:
		if w.uploads > 0 {
			w.uploads--
		}
	}
}

func (w *Worker) pollUploadQueue() {
	w.Logger.Debug().Msg("Starting upload queue polling")
	for {
		time.Sleep(100 * time.Millisecond)
		if w.uploads >= w.Config.MaxUploads {
			continue
		}
		msg, found := w.upQueue.pop()
		if !found { // Queue is empty
			continue
		}
		w.changeUploadCount(1)
		go w.uploadFile(msg.File)
	}
}

// Upload file to bucket
func (w *Worker) uploadFile(srcFile string) {
	defer w.changeUploadCount(-1)
	fileAbs := filepath.Join(w.Config.BasePath, srcFile)
	fileWr, err := os.Open(fileAbs)
	if err != nil {
		w.Logger.Error().Err(err).Msg(eMap["fileAccess"])
		w.mb.StatusUpdate(StatusMsg{MSG_UPLOAD_ERROR, srcFile, hostname, eMap["fileAccess"]})
		return
	}
	defer fileWr.Close()

	w.mb.StatusUpdate(StatusMsg{MSG_UPLOAD_START, srcFile, hostname, ""})
	result, err := w.uploader.upload(fileWr, srcFile)
	if err != nil {
		w.Logger.Error().Err(err).Str("File", fileAbs).Msg(eMap["uploadBucket"])
		w.mb.StatusUpdate(StatusMsg{MSG_UPLOAD_ERROR, srcFile, hostname, eMap["uploadBucket"]})
		return
	}
	w.Logger.Info().Msgf("Uploaded file '%s' to URL: '%s'", fileAbs, result)
	w.mb.StatusUpdate(StatusMsg{MSG_UPLOAD_DONE, srcFile, hostname, ""})
	err = os.Remove(fileAbs)
	if err != nil {
		w.Logger.Error().Err(err).Msgf(eMap["fileDelete"], fileAbs)
	}
	w.Logger.Debug().Msgf("Deleted file '%s'", fileAbs)
}

func (w *Worker) pubErrCb(err error) {
	w.Logger.Fatal().Err(err).Msg(eMap["asyncPub"])
}

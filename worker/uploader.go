package worker

import (
	"io"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
)

func newUploader(bucket string) *uploader {
	u := uploader{
		Bucket: bucket,
	}
	u.getS3Uploader()
	return &u
}

type uploader struct {
	uploader s3manageriface.UploaderAPI
	Bucket   string
}

// Create new S3 uploader
func (t *uploader) getS3Uploader() {
	sess := session.Must(session.NewSession())
	t.uploader = s3manager.NewUploader(sess)
}

// Upload file to S3
func (t *uploader) upload(reader io.Reader, dst string) (string, error) {
	opts := &s3manager.UploadInput{
		Bucket: &t.Bucket,
		Key:    &dst,
		Body:   reader,
	}
	result, err := t.uploader.Upload(opts)
	if err != nil {
		return "", err
	}
	return result.Location, nil
}

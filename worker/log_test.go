package worker

import (
	"testing"

	"github.com/rs/zerolog"

	"github.com/stretchr/testify/assert"
)

func TestGetLogger(t *testing.T) {
	assert := assert.New(t)
	l := wLogger{
		LogLevel: "info",
		LogFile:  "/tmp/test123.log",
	}

	logger, wc, err := l.startLogger()
	assert.Equal(nil, err, "Should not have got an error")
	assert.IsType((*zerolog.Logger)(nil), logger, "Did not get back type logger")
	wc.Close()
}

func TestGetLoggerError(t *testing.T) {
	expected := "Can not open logfile: open /not/a/real/path: no such file or directory"
	l := wLogger{
		LogFile: "/not/a/real/path",
	}
	_, _, err := l.startLogger()
	assert.EqualError(t, err, expected, "Incorrect error")
}

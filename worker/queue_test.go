package worker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQueue(t *testing.T) {
	assert := assert.New(t)

	q := &uploadQueue{}

	// Test pop no msgs
	_, hasMsg := q.pop()
	assert.Equal(false, hasMsg, "Pop hasMsg wrong")

	// Test push first message
	expected := "push1"
	m1 := &DispatchMsg{
		Action: expected,
	}
	q.push(m1)
	assert.Same(m1, q.First.Msg, "m1 - Pointer to first incorrect")
	assert.Same(m1, q.Last.Msg, "m1 - Pointer to last not correct")
	assert.Equal(expected, q.First.Msg.Action, "m1 - Action did not match")
	assert.Equal(1, q.Len, "Len not correct")

	// Test push second message
	expected = "push2"
	m2 := &DispatchMsg{
		Action: expected,
	}
	q.push(m2)
	assert.Same(m2, q.First.Msg, "m2 - Pointer to first incorrect")
	assert.Same(m1, q.Last.Msg, "m2 - Pointer to last not correct")
	assert.Equal(expected, q.First.Msg.Action, "m2 - Action did not match")
	assert.Equal(2, q.Len, "Len not correct")

	// Test pop
	expected = "pop3"
	m3 := &DispatchMsg{
		Action: expected,
	}
	q.push(m3)
	assert.Same(m1, q.Last.Msg, "m3 - Pointer to last not correct")
	assert.Equal(3, q.Len, "Len not correct")

	popped, hasMsg := q.pop()
	assert.Same(m3, q.First.Msg, "m3 - Pointer to first incorrect")
	assert.Same(m1, popped, "m3 - Pointer to popped msg incorrect")
	assert.Same(m2, q.Last.Msg, "m3 - Pointer to last not correct")
	assert.Equal(2, q.Len, "Len not correct")
	assert.Equal("push1", popped.Action, "m3 - Action did not match")
	assert.Equal(true, hasMsg, "m3 - Has msg not correct")
}

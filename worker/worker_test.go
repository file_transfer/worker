package worker

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sync"
	"testing"
	"time"

	nats "github.com/nats-io/go-nats"
	"github.com/stretchr/testify/assert"
)

var natsHost string

var testDisp struct {
	nc *nats.Conn
}

func TestMain(m *testing.M) {
	natsHost = "localhost"
	if os.Getenv("MSG_HOST") != "" {
		natsHost = os.Getenv("MSG_HOST")
	}
	url := fmt.Sprintf("nats://dispatcher:bar@%s:4222", natsHost)
	fmt.Printf("Nats host: %s\n\n", url)
	nc, err := nats.Connect(url)
	if err != nil {
		panic(err)
	}
	testDisp.nc = nc
	result := m.Run()
	nc.Close()
	os.Exit(result)
}

type mockMessageBus struct {
	errConnect string
	errSub     string
	StatusCb   func(StatusMsg)
}

func (m *mockMessageBus) Connect() error {
	if m.errConnect != "" {
		return errors.New(m.errConnect)
	}
	return nil
}

func (m *mockMessageBus) Subscribe() error {
	if m.errSub != "" {
		return errors.New(m.errSub)
	}
	return nil
}

func (m *mockMessageBus) StatusUpdate(msg StatusMsg) {
	m.StatusCb(msg)
}

func (m *mockMessageBus) Close() error {
	return nil
}

type logMsg struct {
	Error string `json:"error"`
	Msg   string `json:"message"`
}

func TestNewWorker(t *testing.T) {
	assert := assert.New(t)

	os.Unsetenv("LOG_FILE")
	w, err := New()
	assert.IsType((*Worker)(nil), w, "New did not create Worker type")
	assert.Equal(nil, err, "Should not have thrown error")
	w.logCloser.Close()

	// Test new worker error
	expected := "Can not open logfile: open /not/a/real/path: no such file or directory"
	os.Setenv("LOG_FILE", "/not/a/real/path")
	_, err = New()
	os.Unsetenv("LOG_FILE")
	assert.EqualError(err, expected, "Logfile should have thrown error")
}

func TestWorkerStart(t *testing.T) {
	assert := assert.New(t)

	// Test worker start should panic
	expected := "Unable to connect to msg bus: host - nats://%s:9999,"
	expected += " nats: no servers available for connection"
	expected = fmt.Sprintf(expected, natsHost)
	w, _ := New()
	w.Config.MsgConfig.Port = 9999
	err := w.Start()

	assert.EqualError(err, expected, "Start did not have correct error")

	// Test worker start normal
	// Write log msg to buffer
	var wr bytes.Buffer
	logger := wLogger{
		LogLevel: "info",
		writer:   &wr,
	}

	mb := &mockMessageBus{}
	w = &Worker{
		Config:    GetOpts(),
		mb:        mb,
		Logger:    logger._newLogger(),
		logCloser: mb, // This is also an io.Closer
	}
	defer w.Stop()

	// Test start does not cause error
	err = w.Start()
	assert.NoError(err, "Start caused an error")

	// Test start error from mb.Subscribe
	mb.errSub = "Air Or"
	err = w.Start()
	assert.EqualErrorf(err, "Unable to subscribe to msg bus: Air Or", "Start did not throw error")

}

func TestSubCb(t *testing.T) {
	assert := assert.New(t)
	expected := "So long, farewell, auf Wiedersehen, good night"

	// Write log msg to buffer
	var wr bytes.Buffer
	logger := wLogger{
		LogLevel: "info",
		writer:   &wr,
	}

	w := Worker{
		Logger:  logger._newLogger(),
		upQueue: &uploadQueue{},
	}

	// Test subCB error
	w.subCB(nil, errors.New(expected))

	var result logMsg
	err := json.Unmarshal(wr.Bytes(), &result)
	assert.NoError(err, "Error from JSON unmarshall")
	assert.Equal(expected, result.Error, "Did not log error message")

	// Test subCB file upload action
	msg := &DispatchMsg{
		Action: "fileUpload",
		File:   "myfile.txt",
	}
	w.subCB(msg, nil)

	assert.Equal(msg.File, w.upQueue.Last.Msg.File, "Incorrect upload file")
	assert.Equal(msg.Action, w.upQueue.Last.Msg.Action, "Incorrect upload action")
}

// Mock io.Writter for logger
type mockWriter struct{}

func (wb *mockWriter) Write(b []byte) (int, error) {
	return len(b), nil
}

func TestPollUploadQueue(t *testing.T) {
	assert := assert.New(t)
	bucket := "test-bucket"

	var wg sync.WaitGroup
	logger := wLogger{
		LogLevel: "debug",
		writer:   &mockWriter{},
	}
	up := uploader{
		Bucket:   bucket,
		uploader: &mockUploader{},
	}
	cfg := GetOpts()
	cfg.BasePath = "../.tmp/upload"

	w := Worker{
		Logger:   logger._newLogger(),
		uploader: &up,
		Config:   cfg,
		upQueue:  &uploadQueue{},
	}
	go w.pollUploadQueue()

	var msg *DispatchMsg

	// Test w.uploads >= w.Config.MaxUploads
	w.uploads = 5
	time.Sleep(300 * time.Millisecond)
	w.uploads = 0

	// Test os.Open error
	msg = &DispatchMsg{
		Action: "fileUpload",
		File:   "my/fake/file",
	}
	w.mb = &mockMessageBus{
		StatusCb: func(ms StatusMsg) {
			wg.Done()
			assert.Equal("Error accessing file for upload", ms.Meta)
		},
	}
	wg.Add(1)
	w.upQueue.push(msg)
	wg.Wait()

	// Test uploader.upload error
	msg = &DispatchMsg{
		Action: "fileUpload",
		File:   "error",
	}
	w.mb = &mockMessageBus{
		StatusCb: func(ms StatusMsg) {
			if ms.Action != MSG_UPLOAD_START { // ignore start msg
				wg.Done()
				assert.Equal("Error trying to upload file to bucket", ms.Meta)
			}
		},
	}
	wg.Add(1)
	w.upQueue.push(msg)
	wg.Wait()

	// Test good mock upload
	msg = &DispatchMsg{
		Action: "fileUpload",
		File:   "unit.txt",
	}
	w.mb = &mockMessageBus{
		StatusCb: func(ms StatusMsg) {
			if ms.Action == MSG_UPLOAD_START {
				wg.Done()
				assert.Equal(MSG_UPLOAD_START, ms.Action, "Incorrect msg action")
			} else {
				wg.Done()
				assert.Equal(MSG_UPLOAD_DONE, ms.Action, "Incorrect msg action")
			}
		},
	}
	wg.Add(2)
	w.upQueue.push(msg)
	wg.Wait()

	// Ensure file unit.txt has been deleted
	expected := "stat unit.txt: no such file or directory"
	_, err := os.Stat("unit.txt")
	assert.EqualError(err, expected, "File was not deleted")
}

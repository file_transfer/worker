package worker

import (
	"errors"
	"fmt"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"

	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/stretchr/testify/assert"
)

func TestGetUploader(t *testing.T) {
	u := uploader{}
	u.getS3Uploader()
	assert.IsType(t, (*s3manager.Uploader)(nil), u.uploader, "Not an uploader type")
}

type mockUploader struct {
	s3manageriface.UploaderAPI
}

func (m *mockUploader) Upload(
	ui *s3manager.UploadInput, _ ...func(*s3manager.Uploader)) (*s3manager.UploadOutput, error) {
	if *ui.Key == "error" {
		return nil, errors.New("Houston, we have a problem.")
	}

	loc := fmt.Sprintf("https://%s.s3.us-west-2.amazonaws.com/%s", *ui.Bucket, *ui.Key)
	result := &s3manager.UploadOutput{
		Location: loc,
	}

	return result, nil
}

func TestUpload(t *testing.T) {
	assert := assert.New(t)
	file := "test.txt"
	bucket := "test-bucket"
	expected := fmt.Sprintf("https://%s.s3.us-west-2.amazonaws.com/%s", bucket, file)
	reader := strings.NewReader("hello")
	u := uploader{
		Bucket:   bucket,
		uploader: &mockUploader{},
	}
	location, err := u.upload(reader, file)
	assert.NoError(err, "Error thrown in upload")
	assert.Equal(expected, location, "Output did not match")
}

func TestUploadError(t *testing.T) {
	assert := assert.New(t)
	expected := "Houston, we have a problem."
	file := "error"
	u := uploader{
		uploader: &mockUploader{},
	}
	_, err := u.upload(nil, file)
	assert.EqualError(err, expected, "Error not thrown in upload")
}

package worker

import (
	"encoding/json"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/caarlos0/env"
	nats "github.com/nats-io/go-nats"

	"github.com/stretchr/testify/assert"
)

type mockMsgConsumer struct {
	subcb    func(*DispatchMsg, error)
	pubErrcb func(error)
}

func (m *mockMsgConsumer) subCB(d *DispatchMsg, err error) {
	if m.subcb != nil {
		m.subcb(d, err)
	}
}

func (m *mockMsgConsumer) pubErrCb(err error) {
	m.pubErrcb(err)
}

func TestConnect(t *testing.T) {
	assert := assert.New(t)

	mc := &MsgConfig{
		User: "worker",
		Pass: "foo",
	}

	err := env.Parse(mc)
	assert.Equal(nil, err, "Env parse error")

	mb := &messageBus{
		config:      mc,
		msgConsumer: &mockMsgConsumer{},
	}

	err = mb.Connect()
	assert.Equal(nil, err, "Error on Connect")
	mb.Close()
}

func TestConnectError(t *testing.T) {
	assert := assert.New(t)

	mc := &MsgConfig{
		User: "worker",
		Pass: "foo",
		Host: natsHost,
	}

	err := env.Parse(mc)
	assert.Equal(nil, err, "Env parse error")
	mb := &messageBus{
		config:      mc,
		msgConsumer: &mockMsgConsumer{},
		encoder:     "notAnEnc",
	}

	// Test NewEncodedConn error
	expected := "Encoded conn err: no encoder registered for 'notAnEnc'"
	err = mb.Connect()
	assert.EqualError(err, expected, "Error incorrect")
	mb.Close()

	// Test nats.Connect error
	mc.TLS = true
	expected = "host - tls://%s:4222, nats: secure connection not available"
	err = mb.Connect()
	assert.EqualError(err, fmt.Sprintf(expected, natsHost), "Error incorrect")
}

func TestSub(t *testing.T) {
	assert := assert.New(t)
	expected1 := "fileUpload"
	expected2 := "hello.txt"
	mc := &MsgConfig{
		User: "worker",
		Pass: "foo",
		Host: natsHost,
		Port: 4222,
	}

	err := env.Parse(mc)
	assert.Equal(nil, err, "Env parse error")

	var mb *messageBus

	// Test normal sub operation
	wg := sync.WaitGroup{}
	wg.Add(1)
	TestCb := &mockMsgConsumer{
		subcb: func(msg *DispatchMsg, err error) {
			assert.Equal(expected1, msg.Action, "File action did not match")
			assert.Equal(expected2, msg.File, "File name did not match")
			wg.Done()
		},
	}

	mb = &messageBus{
		config:      mc,
		msgConsumer: TestCb,
	}

	err = env.Parse(mc)
	assert.Equal(nil, err, "Env parse error")

	err = mb.Connect()
	assert.Equal(err, nil, "Error on Connect")

	err = mb.Subscribe()
	assert.Equal(err, nil, "Error on Subscribe")
	time.Sleep(time.Millisecond) // Allow time for sub to happen

	msg, _ := json.Marshal(DispatchMsg{
		Action: expected1,
		File:   expected2,
	})
	err = testDisp.nc.Publish("workers.queue", msg)
	assert.Equal(nil, err, "Error thrown by pub")
	wg.Wait()

	// Test sub error
	wg.Add(1)
	expected3 := "Invalid dispatcher action: none"
	TestCb2 := &mockMsgConsumer{
		subcb: func(_ *DispatchMsg, err error) {
			assert.EqualError(err, expected3, "Incorrect error message")
			mb.Close()
			wg.Done()
		},
	}

	mb.sub.Unsubscribe()
	mb.msgConsumer = TestCb2
	mb.Subscribe()
	time.Sleep(time.Millisecond) // Allow time for sub to happen

	msg2, _ := json.Marshal(DispatchMsg{Action: "none"})
	err = testDisp.nc.Publish("workers.queue", msg2)
	assert.Equal(nil, err, "Error thrown by pub")
	wg.Wait()
}

func TestStatusUpdate(t *testing.T) {
	assert := assert.New(t)

	mc := &MsgConfig{
		User: "worker",
		Pass: "foo",
		Host: natsHost,
	}
	err := env.Parse(mc)
	assert.Equal(nil, err, "Env parse error")

	mcsmr := &mockMsgConsumer{
		pubErrcb: func(err error) {
			assert.Equal(err, nil, "Error in pub message")
		},
	}

	mb := &messageBus{
		config:       mc,
		msgConsumer:  mcsmr,
		statChanSize: 4,
	}

	err = mb.Connect()
	assert.Equal(err, nil, "Error on Connect")

	wg := sync.WaitGroup{}
	wg.Add(1)
	expected := StatusMsg{"uploadDone", "test.txt", "meMyselfAndIrene", "hi"}

	dispCb := func(m *nats.Msg) {
		var msg StatusMsg
		err := json.Unmarshal(m.Data, &msg)
		assert.Equal(err, nil, "Error parsing message")
		assert.Equal(expected, msg, "Message did not match")
		wg.Done()
	}

	_, err2 := testDisp.nc.Subscribe("workers.status", dispCb)
	assert.Equal(err2, nil, "Error with subscribe")
	time.Sleep(10 * time.Millisecond)

	mb.StatusUpdate(expected)
	wg.Wait()

	// Test async pub error
	mb.nc.Close() // Close nats conn, but leave nc pointer intact
	expected2 := "Error with async pub: 'nats: connection closed'"
	wg.Add(1)
	mb.msgConsumer = &mockMsgConsumer{
		pubErrcb: func(err error) {
			assert.EqualError(err, expected2, "Error in pub message")
			wg.Done()
		},
	}

	mb.StatusUpdate(StatusMsg{})
	wg.Wait()

}

package worker

import (
	"errors"
	"fmt"

	nats "github.com/nats-io/go-nats"
)

const (
	queueTopic  = "workers.queue"
	statusTopic = "workers.status"
)

// Abstracts the specifics of the NATS message bus from the rest of the worker
type messageBusI interface {
	Connect() error
	Subscribe() error
	Close() error
	StatusUpdate(StatusMsg)
}

type messageBus struct {
	nc           *nats.EncodedConn
	sub          *nats.Subscription
	msgConsumer  msgConsumer
	config       *MsgConfig
	encoder      string
	DispActions  DispatchActions
	UpActions    UpdateActions
	statChan     chan StatusMsg
	statChanSize int // Make *4 max uploads
}

// Main worker struct should have SubCB pubErrCb methods
type msgConsumer interface {
	subCB(*DispatchMsg, error)
	pubErrCb(error)
}

// Message recieved from dispatcher
type DispatchMsg struct {
	Action string `json:"action"`
	File   string `json:"file"`
}

// Map of all dispatcher actions supported by the worker
type DispatchActions map[string]bool

func getDispActions() DispatchActions {
	return DispatchActions{
		"fileUpload": true,
	}
}

// Connect to message bus
func (m *messageBus) Connect() error {
	proto := "nats"
	if m.config.TLS {
		proto = "tls"
	}

	creds := ""
	if m.config.User != "" {
		creds = m.config.User
		if m.config.Pass != "" {
			creds += ":" + m.config.Pass
		}
		creds += "@"
	}
	url := fmt.Sprintf("%s://%s%s:%d", proto, creds, m.config.Host, m.config.Port)
	nc, err := nats.Connect(url)
	if err != nil {
		// Safe url string
		return fmt.Errorf("host - %s://%s:%d, %v", proto, m.config.Host, m.config.Port, err)
	}
	if m.encoder == "" {
		m.encoder = nats.JSON_ENCODER
	}
	enc, err := nats.NewEncodedConn(nc, m.encoder)
	if err != nil {
		return fmt.Errorf("Encoded conn err: %v", err)
	}
	m.nc = enc

	m.statChan = make(chan StatusMsg, m.statChanSize)
	m.startAsyncPub()
	m.DispActions = getDispActions()
	m.UpActions = getUpdateActions()
	return nil
}

func (m *messageBus) Subscribe() error {
	var err error
	m.sub, err = m.nc.QueueSubscribe(queueTopic, "worker", func(dm *DispatchMsg) {
		if !m.DispActions[dm.Action] {
			m.msgConsumer.subCB(nil, errors.New("Invalid dispatcher action: "+dm.Action))
			return
		}
		m.msgConsumer.subCB(dm, nil)
	})
	return err
}

// List of actions that can be sent in update message
type UpdateActions map[string]bool

var (
	MSG_UPLOAD_START    string = "uploadStart"
	MSG_UPLOAD_PROGRESS string = "uploadProgress"
	MSG_UPLOAD_DONE     string = "uploadDone"
	MSG_UPLOAD_ERROR    string = "uploadError"
)

func getUpdateActions() UpdateActions {
	return UpdateActions{
		MSG_UPLOAD_START:    true,
		MSG_UPLOAD_PROGRESS: true,
		MSG_UPLOAD_DONE:     true,
		MSG_UPLOAD_ERROR:    true,
	}
}

// Status message to be sent to dispatcher
type StatusMsg struct {
	Action string `json:"action"`
	File   string `json:"file"`
	Worker string `json:"worker"`
	Meta   string `json:"meta,omitempty"`
}

// Send StatusMsg to dispatcher
func (m *messageBus) StatusUpdate(s StatusMsg) {
	m.statChan <- s
}

func (m *messageBus) startAsyncPub() {
	go func(ch chan StatusMsg) {
		for {
			msg := <-ch
			err := m.nc.Publish(statusTopic, msg)
			if err != nil {
				m.msgConsumer.pubErrCb(fmt.Errorf("Error with async pub: '%v'", err))
			}
		}
	}(m.statChan)
}

// Closes connection to message bus server
func (m *messageBus) Close() error {
	if m.nc != nil {
		m.nc.Close()
		m.nc = nil
	}
	return nil
}

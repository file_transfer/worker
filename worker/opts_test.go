package worker

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetOptsDefault(t *testing.T) {
	assert := assert.New(t)
	// Test a handful of default values
	o := GetOpts()

	assert.Equal("/tmp/worker.log", o.LogFile, "Default logfile did not match.")
	assert.Equal(uint16(4), o.MaxUploads, "Default maxuploads did not match.")
	assert.Equal(4222, o.MsgConfig.Port, "Default msg host did not match.")
}

func TestGetOpts(t *testing.T) {
	assert := assert.New(t)
	expected := "/path/to/file"
	// Test a handful of set values
	os.Setenv("LOG_FILE", expected)
	os.Setenv("MSG_TLS", "true")
	o := GetOpts()

	os.Unsetenv("LOG_FILE")
	os.Unsetenv("MSG_TLS")

	assert.Equal(expected, o.LogFile, "Default logfile did not match.")
	assert.True(o.MsgConfig.TLS, "Default TLS did not match.")
}

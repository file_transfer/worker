package worker

import (
	"sync"
)

type node struct {
	Msg  *DispatchMsg
	Prev *node
	Next *node
}

type uploadQueue struct {
	First *node
	Last  *node
	Len   int
	mu    sync.Mutex
}

func (q *uploadQueue) push(m *DispatchMsg) {
	q.mu.Lock()
	defer q.mu.Unlock()
	first := &node{
		Msg:  m,
		Prev: nil,
		Next: q.First,
	}
	if q.First != nil {
		q.First.Prev = first
	}
	q.First = first
	q.Len++
	if q.Last == nil {
		q.Last = first
	}
}

func (q *uploadQueue) pop() (*DispatchMsg, bool) {
	q.mu.Lock()
	defer q.mu.Unlock()
	// Check if queue has any messages
	if q.Last == nil {
		return nil, false
	}
	msg, last := q.Last.Msg, q.Last
	// Change last node in list
	q.Last, q.Last.Next = last.Prev, nil
	// Clear current node
	last.Msg, last.Prev, last.Next = nil, nil, nil
	q.Len--
	return msg, true
}

package worker

var (
	eMap = map[string]string{
		"msgBusConnect":  "Unable to connect to msg bus: %v",
		"msgBusSubcribe": "Unable to subscribe to msg bus: %v",
		"msgDisp":        "Message dispatch error",
		"fileAccess":     "Error accessing file for upload",
		"uploadBucket":   "Error trying to upload file to bucket",
		"asyncPub":       "Error with async message pub",
		"fileDelete":     "Error deleting file: %s",
	}
)

package worker

import (
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/rs/zerolog"
)

type levelMap map[string]zerolog.Level

func getLevels() levelMap {
	return levelMap{
		"debug": zerolog.DebugLevel,
		"info":  zerolog.InfoLevel,
		"warn":  zerolog.WarnLevel,
		"error": zerolog.ErrorLevel,
		"fatal": zerolog.FatalLevel,
		"panic": zerolog.PanicLevel,
	}
}

// Thread safe Writer
type muWriter struct {
	file io.Writer
	mu   sync.Mutex
}

func (wc *muWriter) Write(b []byte) (int, error) {
	wc.mu.Lock()
	defer wc.mu.Unlock()
	return wc.file.Write(b)
}

type wLogger struct {
	writer   io.Writer
	LogLevel string
	LogFile  string
}

func (l *wLogger) startLogger() (*zerolog.Logger, io.Closer, error) {
	file, err := os.OpenFile(l.LogFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, nil, fmt.Errorf("Can not open logfile: %v", err)
	}
	l.writer = &muWriter{file: file}
	return l._newLogger(), file, nil
}

// Seperated from startLogger for easier unit testing elsewhere in worker
func (l *wLogger) _newLogger() *zerolog.Logger {
	lvl := getLevels()[l.LogLevel]
	logger := zerolog.
		New(l.writer).
		Level(lvl).
		With().
		Timestamp().
		Str("hostname", hostname).
		Logger()
	return &logger
}

package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/file_transfer/worker/worker"
)

func main() {
	w, err := worker.New()
	if err != nil {
		log.Fatalf("Error creating new worker: '%v'", err)
	}
	err = w.Start()
	if err != nil {
		log.Fatalf("Error starting worker: '%v'", err)
	}

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	sig := <-ch

	w.Logger.Info().Msgf("Got signal %s, shutting down.", sig)
	w.Stop()
}

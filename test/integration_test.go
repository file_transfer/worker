package test

import (
	"fmt"
	"log"
	"os"
	"sync"
	"testing"
	"time"

	"../worker"
	"github.com/nats-io/go-nats"
	"github.com/stretchr/testify/assert"
)

var (
	natsHost    string
	wk          *worker.Worker
	dispTopic   string = "workers.queue"
	statusTopic string = "workers.status"
)

var testDisp struct {
	nc *nats.EncodedConn
}

func TestMain(m *testing.M) {
	os.Setenv("MSG_USER", "worker")
	os.Setenv("MSG_PASS", "foo")
	os.Setenv("UPLOAD_DIR", "../.tmp/upload")
	os.Setenv("LOG_FILE", "../.tmp/worker.log")
	os.Setenv("LOG_LEVEL", "debug")

	// Turn off worker start in this file
	testBinary := false
	if os.Getenv("BIN_TEST") != "" {
		testBinary = true
	}

	natsHost = "localhost"
	if os.Getenv("MSG_HOST") != "" {
		natsHost = os.Getenv("MSG_HOST")
	}
	url := fmt.Sprintf("nats://dispatcher:bar@%s:4222", natsHost)
	fmt.Printf("Nats host: %s\n\n", url)
	nc, err := nats.Connect(url)
	if err != nil {
		log.Fatalf("Error starting NATS test dispatcher: '%v'", err)
	}
	enc, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		log.Fatalf("Error starting NATS test dispatcher: '%v'", err)
	}
	testDisp.nc = enc

	if !testBinary {
		w, err := worker.New()
		if err != nil {
			log.Fatalf("Error creating new worker: '%v'", err)
		}
		err = w.Start()
		if err != nil {
			log.Fatalf("Error starting worker: '%v'", err)
		}
		wk = w
	}

	time.Sleep(time.Second) // Let everything catch up
	result := m.Run()

	if wk != nil {
		wk.Stop()
	}
	nc.Close()
	os.Exit(result)
}

func TestWorker(t *testing.T) {
	assert := assert.New(t)
	var wg sync.WaitGroup
	var assertCb func(*worker.StatusMsg)

	dispSubCb := func(m *worker.StatusMsg) {
		assertCb(m)
	}
	sub, err := testDisp.nc.Subscribe(statusTopic, dispSubCb)
	if err != nil {
		assert.FailNow("Error with disp sub", err)
	}

	// Test file upload
	wg.Add(2)
	file := "./test.txt"
	assertCb = func(m *worker.StatusMsg) {
		if m.Action == worker.MSG_UPLOAD_START {
			assert.Equal(worker.MSG_UPLOAD_START, m.Action, "Dispatcher did not get right action")
			assert.Equal(file, m.File, "Dispatcher did not get right file")
			wg.Done()
		} else {
			assert.Equal(worker.MSG_UPLOAD_DONE, m.Action, "Dispatcher did not get right action: ", m.Meta)
			assert.Equal(file, m.File, "Dispatcher did not get right file")
			wg.Done()
		}
	}

	msg := worker.DispatchMsg{
		Action: "fileUpload",
		File:   file,
	}
	err = testDisp.nc.Publish(dispTopic, msg)
	assert.NoError(err, "Should not have thrown error")
	wg.Wait()

	// Test error with file upload
	wg.Add(1)
	file = "./no_file_there.txt"
	meta := "Error accessing file for upload"
	assertCb = func(m *worker.StatusMsg) {
		assert.Equal(worker.MSG_UPLOAD_ERROR, m.Action, "Dispatcher did not get right action")
		assert.Equal(file, m.File, "Dispatcher did not get right action")
		assert.Equal(meta, m.Meta, "Dispatcher did not get right action")
		wg.Done()
	}

	msg = worker.DispatchMsg{
		Action: "fileUpload",
		File:   file,
	}
	err = testDisp.nc.Publish(dispTopic, msg)
	assert.NoError(err, "Should not have thrown error")
	wg.Wait()

	sub.Unsubscribe()
}

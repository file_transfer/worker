# Worker Microservice

This repo is a microservice written in Go that handles the file upload when it receives a message from the dispatcher. An overview of the software design is shown in the diagram below.

![design](worker.png)



<img class="statcounter" src="https://c.statcounter.com/11994331/0/eed1a29e/1/">